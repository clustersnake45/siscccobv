unit main;

{$mode objfpc}{$H+}

interface

uses
  Forms, ExtCtrls, Menus, StdCtrls, Classes,
  Buttons;

type

  { TmainFrm }

  TmainFrm = class(TForm)
      Bevel1: TBevel;
      GroupBox1: TGroupBox;
      GroupBox2: TGroupBox;
      Notebook1: TNotebook;
      Page1: TPage;
      Page2: TPage;
      Page3: TPage;
      Page4: TPage;
      Panel1: TPanel;
      Panel2: TPanel;
      comunityBtn: TSpeedButton;
      organizationBtn: TSpeedButton;
      institutionsBtn: TSpeedButton;
      helpBtn: TSpeedButton;
      exitBtn: TSpeedButton;
      SpeedButton1: TSpeedButton;
      SpeedButton2: TSpeedButton;
      SpeedButton3: TSpeedButton;
      SpeedButton4: TSpeedButton;
      SpeedButton5: TSpeedButton;
      SpeedButton6: TSpeedButton;
      SpeedButton7: TSpeedButton;
      SpeedButton8: TSpeedButton;
      procedure comunityBtnClick(Sender: TObject);
      procedure organizationBtnClick(Sender: TObject);
      procedure institutionsBtnClick(Sender: TObject);
      procedure exitBtnClick(Sender: TObject);
      procedure SpeedButton1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  mainFrm: TmainFrm;

implementation

{$R *.lfm}

{ TmainFrm }

procedure TmainFrm.comunityBtnClick(Sender: TObject);
begin
   Notebook1.PageIndex:=0;
end;

procedure TmainFrm.organizationBtnClick(Sender: TObject);
begin
   Notebook1.PageIndex:=1;
end;

procedure TmainFrm.institutionsBtnClick(Sender: TObject);
begin
    Notebook1.PageIndex:=2;
end;

procedure TmainFrm.exitBtnClick(Sender: TObject);
begin
    mainFrm.Close;
end;

procedure TmainFrm.SpeedButton1Click(Sender: TObject);
begin

end;

end.

