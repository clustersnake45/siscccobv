unit splash;

{$mode objfpc}{$H+}

interface

uses
  Classes, Forms, ExtCtrls,
  ComCtrls;

type

  { Tsplash_frm }

  Tsplash_frm = class(TForm)
    Image1: TImage;
    Panel1: TPanel;
    ProgressBar1: TProgressBar;
    Timer1: TTimer;

    procedure FormCreate(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  splash_frm: Tsplash_frm;
  contador: Integer;
implementation

{$R *.lfm}

{ Tsplash_frm }

procedure Tsplash_frm.FormCreate(Sender: TObject);
begin
  contador := 0;
end;

procedure Tsplash_frm.Image1Click(Sender: TObject);
begin

end;

procedure Tsplash_frm.Timer1Timer(Sender: TObject);
begin

  ProgressBar1.Position:= ProgressBar1.Position + 10;

  if contador = 15 then
     Close;
  contador := contador + 1;
end;

end.

